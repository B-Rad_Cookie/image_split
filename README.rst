Image Split
===========

Welcome the Image Split: the tool which allows you to to create a series of images which are a sub region of an overall image. The tool is written in C# and supports the latest .NET Framework.

Documentation
-------------

The user documentation can be found in doc/Image_Split_User_Doc.pdf.