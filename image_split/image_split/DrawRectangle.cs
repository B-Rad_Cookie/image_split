﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    /// <summary>
    /// Contains the handling of drawing a rectangle as a boundary of a
    /// sub-image. It handles the three stages required for drawing a
    /// rectangle: MouseDown, MouseMove and MouseUp.
    /// </summary>
    /// <remarks>
    /// As the methods in this class are designed to be delegate subscribers
    /// and this class is representing a editing "state", the purpose of the
    /// class is a utility class to provide functionalities for drawing
    /// rectangles. As such, all members required in the drawing of a
    /// rectangle will be a static on the class.
    /// </remarks>
    static class DrawRectangle
    {
        /*---------------------------------------------------------------------
                              Public Static members
         --------------------------------------------------------------------*/
        /// <summary>
        /// The Rectangle to draw.
        /// </summary>
        public static Rectangle drawnRectangle;

        /// <summary>
        /// Determines whether a new rectangle is being drawn.
        /// </summary>
        public static bool isDrawing = false;

        /// <summary>
        /// The MouseDown event handler for drawing a rectangle. Creates a new
        /// rectangle. This method is designed to be subscribed to the
        /// MouseDownEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public static bool RectangleMouseDownEvent(ListView subImageListView,
                                                   SubImageList subImages,
                                                   MouseEventArgs e,
                                                   Point mouseStart,
                                                   float xScale, float yScale,
                                                   string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the rectangle button
            // "editRectangleButton", do nothing.
            if (editState != "editRectangleButton")
            {
                return false;
            }

            // Set the isDrawing to true
            DrawRectangle.isDrawing = true;

            // The only two things required for drawing a Rectangle is setting the
            // start position and creating the Rectangle object. As the first
            // step is done in the Form.cs MouseDown, this just needs to create
            // the Rectangle.
            DrawRectangle.drawnRectangle = new Rectangle(mouseStart,
                                                         new Size(0, 0));
            // No redraw required.
            return false;
        }

        /// <summary>
        /// The MouseUp event handler for drawing a rectangle. Saves the
        /// rectangle shape to the selected sub-image. This method is designed
        /// to be subscribed to the MouseUpEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        public static bool RectangleMouseUpEvent(ListView subImageListView,
                                                 SubImageList subImages,
                                                 MouseEventArgs e,
                                                 Point mouseStart,
                                                 float xScale, float yScale,
                                                 string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the rectangle button
            // "editRectangleButton", do nothing.
            if (editState != "editRectangleButton")
            {
                return false;
            }

            // When the Rectangle is finished drawing, we need to get the
            // points of the corners (in pixels).
            Point topLeft = 
                new Point((int)(DrawRectangle.drawnRectangle.Left * xScale),
                          (int)(DrawRectangle.drawnRectangle.Top * yScale));
            Point topRight =
                new Point((int)(DrawRectangle.drawnRectangle.Right * xScale),
                          (int)(DrawRectangle.drawnRectangle.Top * yScale));
            Point bottomLeft =
                new Point((int)(DrawRectangle.drawnRectangle.Left * xScale),
                          (int)(DrawRectangle.drawnRectangle.Bottom * yScale));
            Point bottomRight =
                new Point((int)(DrawRectangle.drawnRectangle.Right * xScale),
                          (int)(DrawRectangle.drawnRectangle.Bottom * yScale));

            // These then need to be added to the selected sub-image.
            SubImage selected = 
                subImages.Find(subImageListView.SelectedItems[0].Text);
            selected.m_region = new List<Point>();
            selected.m_region.AddRange(new Point[] {topLeft,
                                                    topRight,
                                                    bottomRight,
                                                    bottomLeft});

            // Set the isDrawing to false
            DrawRectangle.isDrawing = false;

            return true;
        }

        /// <summary>
        /// The MouseMove event handler for drawing a rectangle. Allows the
        /// rectangle to be stretched. This method is designed to be
        /// subscribed to the MouseMoveEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public static bool RectangleMouseMoveEvent(ListView subImageListView,
                                                   SubImageList subImages,
                                                   MouseEventArgs e,
                                                   Point mouseStart,
                                                   float xScale, float yScale,
                                                   string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the rectangle button
            // "editRectangleButton", do nothing.
            if (editState != "editRectangleButton")
            {
                return false;
            }

            // Mouse Move here is just simply stretching the rectangle.
            // However, we want the user to be able to stretch in any
            // direction. This means that Rectangle properties will need to be
            // altered to accommodate this depending on where it is being
            // stretched from.

            // If the mouse location is less than the starting point, make the
            // rectangle x the mouse location and the width to stretch to the
            // mouse start point. Otherwise, set the rectangle x to the mouse
            // start and set the width to the distance between the mouse
            // location and mouse start location.
            if (e.Location.X < mouseStart.X)
            {
                DrawRectangle.drawnRectangle.X = e.Location.X;
                DrawRectangle.drawnRectangle.Width = mouseStart.X - DrawRectangle.drawnRectangle.X;
            }
            else
            {
                DrawRectangle.drawnRectangle.X = mouseStart.X;
                DrawRectangle.drawnRectangle.Width = e.Location.X - mouseStart.X;
            }

            // Do the same in Y as with X.
            if (e.Location.Y < mouseStart.Y)
            {
                DrawRectangle.drawnRectangle.Y = e.Location.Y;
                DrawRectangle.drawnRectangle.Height = mouseStart.Y - DrawRectangle.drawnRectangle.Y;
            }
            else
            {
                DrawRectangle.drawnRectangle.Y = mouseStart.Y;
                DrawRectangle.drawnRectangle.Height = e.Location.Y - mouseStart.Y;
            }

            return true;
        }
    }
}
