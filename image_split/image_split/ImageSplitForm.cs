﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    public partial class ImageSplitForm : Form
    {
        /*---------------------------------------------------------------------
                                  Private Members
        ---------------------------------------------------------------------*/
        /// <summary>
        ///  The image being edited.
        /// </summary>
        private Bitmap m_originalImage;

        /// <summary>
        /// Determines whether an image to edit has been loaded into the
        /// application.
        /// </summary>
        private bool m_isImageLoaded;

        /// <summary>
        /// The list of sub-images.
        /// </summary>
        private SubImageList m_subImageList;

        /// <summary>
        /// The scaling factor of the original image to the image canvas
        /// (x-plane).
        /// </summary>
        private float m_xScale;

        /// <summary>
        /// The scaling factor of the original image to the image canvas
        /// (y-plane).
        /// </summary>
        private float m_yScale;

        /// <summary>
        /// The zoom level. 
        /// </summary>
        private int m_zoomLevel;

        /// <summary>
        /// Determines whether the middle mouse button was pressed. This
        /// boolean trigger allows panning to be simultaneously to drawing.
        /// </summary>
        private bool m_middlePressed;

        /// <summary>
        /// Stores the location of the mouse starting location. Used for various
        /// purposes (see the Mouse Down event handler).
        /// </summary>
        private Point m_mouseStart;

        /// <summary>
        /// Stores the location of the panning starting location. Used when
        /// panning.
        /// </summary>
        private Point m_panStart;

        /// <summary>
        /// File path to the image file being edited.
        /// </summary>
        private string m_imageFilePath;

        /// <summary>
        /// File path to the xml file to serialise and deserialise data to and
        /// from.
        /// </summary>
        private string m_xmlFilePath;

        /// <summary>
        /// Colour of the pen to draw the sub-image polygons.
        /// </summary>
        private Color m_lineColour;

        /// <summary>
        /// The font to use for the SubImage labels.
        /// </summary>
        private Font m_labelFont;

        /// <summary>
        /// Stores the identifier of a SubImage about to be edited.
        /// </summary>
        private string m_currentIdentifier;

        /// <summary>
        /// The delegate handler for handling the different editing states for
        /// the Mouse Down event.
        /// </summary>
        private MouseDownEvent m_mouseDownDelegate;

        /// <summary>
        /// The delegate handler for handling the different editing states for
        /// the Mouse Up event.
        /// </summary>
        private MouseUpEvent m_mouseUpDelegate;

        /// <summary>
        /// The delegate handler for handling the different editing states for
        /// the Mouse Move event.
        /// </summary>
        private MouseMoveEvent m_mouseMoveDelegate;

        /*---------------------------------------------------------------------
                              Public Class Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The list of valid image file extensions.
        /// </summary>
        public static List<string> validExtensions = new List<string>()
        { "bmp", "gif", "exif", "jpg", "png", "tiff" };

        /*---------------------------------------------------------------------
                                   Constructors
        ---------------------------------------------------------------------*/

        /// <summary>
        /// Initialises the Application. Sets the Winforms properties, and
        /// initialises some member variables.
        /// </summary>
        public ImageSplitForm()
        {
            InitializeComponent();

            // Initialise member variables (ones that won't be set to null).
            m_isImageLoaded = false;
            m_lineColour = Color.Black;
            m_labelFont = new Font("Calibri", 12.0f, FontStyle.Bold,
                                   GraphicsUnit.Point);
            m_mouseStart = new Point(0, 0);
            m_subImageList = new SubImageList();
            m_middlePressed = false;

            // Set up delegates
            m_mouseDownDelegate = new MouseDownEvent();
            m_mouseUpDelegate = new MouseUpEvent();
            m_mouseMoveDelegate = new MouseMoveEvent();

            // MouseDown subscription
            m_mouseDownDelegate.Subscribe(DrawRectangle.RectangleMouseDownEvent);
            m_mouseDownDelegate.Subscribe(DrawPolygon.PolygonMouseDownEvent);
            m_mouseDownDelegate.Subscribe(DrawFreeForm.FreeFormMouseDownEvent);

            // MouseUp subscription
            m_mouseUpDelegate.Subscribe(DrawRectangle.RectangleMouseUpEvent);
            m_mouseUpDelegate.Subscribe(DrawFreeForm.FreeFormMouseUpEvent);

            // MouseMove subscription.
            m_mouseMoveDelegate.Subscribe(DrawRectangle.RectangleMouseMoveEvent);
            m_mouseMoveDelegate.Subscribe(DrawFreeForm.FreeFormMouseMoveEvent);
        }

        /**********************************************************************
                                 Private Methods
        **********************************************************************/

        /*---------------------------------------------------------------------
                              Main Menu Event Handlers
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Loads an image using the "Open Image" menu item.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the menu item click
        /// event</param>
        private void openImageMenuItem_Click(object sender, EventArgs e)
        {
            // Open a file using the open file dialog box.
            OpenFileDialog openImage = new OpenFileDialog();

            // Set the dialog box properties.
            openImage.Filter = BuildImageFilterString();
            openImage.FilterIndex = 1;
            openImage.RestoreDirectory = true;
            openImage.CheckPathExists = true;
            // Run the dialog box and load the image.
            if (openImage.ShowDialog() == DialogResult.OK)
            {
                LoadImage(openImage.FileName);
            }
        }

        /// <summary>
        /// Loads in the SubImageList information and loads in the attached
        /// image using the "Load Sub-Image XML File" menu item.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the menu item click
        /// event</param>
        private void openXmlMenuItem_Click(object sender, EventArgs e)
        {
            // Use an Open File dialog to get the .xml file. Set the filter to
            // only accept .xml.
            OpenFileDialog openXml = new OpenFileDialog();
            openXml.CheckFileExists = true;
            openXml.Title = "Open Sub-Image XML File";
            openXml.Filter = "XML (*.xml)|*.xml";
            openXml.FilterIndex = 1;

            // If a file is selected, attempt to deserialise the information.
            // If the deserialisation fails, display an error box and do
            // nothing.
            if (openXml.ShowDialog() == DialogResult.OK)
            {
                // Save a copy of the original SubImageList. If the load fails,
                // revert the SubImageList back to it's previous state.
                SubImageList temp = m_subImageList;
                try
                {
                    m_subImageList = SubImageList.Deserialise(openXml.FileName);
                }
                catch (Exception)
                {
                    string errorMsg = "Failed to load in the sub-image ";
                    errorMsg += string.Format("information from the file {0}.",
                        openXml.FileName);
                    errorMsg += " Please ensure the .xml file chosen is one ";
                    errorMsg += "that can be used with this application (such";
                    errorMsg += "as one created by this application).";
                    MessageBox.Show(errorMsg, "Error: Cannot read XML File",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    // Reset the SubImageList back to it's original, then
                    // return.
                    m_subImageList = temp;
                    return;
                }

                // On successful load, load in the image, set xml save file
                // path, and load up the sub-images to the ListView.
                LoadImage(m_subImageList.m_imageFilePath);
                m_xmlFilePath = openXml.FileName;
                PopulateSubImageList();
            }
        }

        /// <summary>
        /// Handles the "Save As" saving of XML information. Writes
        /// (Serialises) the sub-image information to the chosen output file.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the menu item click
        /// event</param>
        private void saveAsMenuItem_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Use a SaveFileDialog. Set the filter to only allow .xml.
            SaveFileDialog saveXmlFile = new SaveFileDialog();
            saveXmlFile.CheckPathExists = true;
            saveXmlFile.Title = "Save Sub-Image Information";
            saveXmlFile.OverwritePrompt = true;
            saveXmlFile.AddExtension = true;
            saveXmlFile.Filter = "XML (*.xml)|*.xml";
            saveXmlFile.FilterIndex = 1;

            // If a file is selected, try to  write the sub-image list
            // information and set the xmlFilePath to the new file. If it
            // fails, display that it failed and why, then return.
            if (saveXmlFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    m_subImageList.Serialise(saveXmlFile.FileName,
                                             m_imageFilePath);
                }
                catch (Exception exception)
                {
                    string errorMsg = "There was an error writing the ";
                    errorMsg += string.Format("information to {0}.",
                                              m_xmlFilePath);
                    errorMsg += string.Format(" The error was:{0}",
                                              Environment.NewLine);
                    errorMsg += exception.Message;
                    MessageBox.Show(errorMsg, "Error: Can't write to XML File",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    // simply return
                    return;
                }

                // On successful write, set the xml path to the new file.
                m_xmlFilePath = saveXmlFile.FileName;
            }
        }

        /// <summary>
        /// Handles the "Save" saving of XML information. Writes (Serialises)
        /// the sub-image information to the already selected output file.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the menu item click
        /// event</param>
        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // If the xml file path is not already set, then call the Save As
            // method;
            if (m_xmlFilePath == null)
            {
                saveAsMenuItem_Click(sender, e);
            }
            else  // Otherwise, write straight to the file.
            {
                try
                {
                    m_subImageList.Serialise(m_xmlFilePath,
                                             m_imageFilePath);
                }
                catch (Exception exception)
                {
                    string errorMsg = "There was an error writing the ";
                    errorMsg += string.Format("information to {0}.",
                                              m_xmlFilePath);
                    errorMsg += string.Format(" The error was:{0}",
                                              Environment.NewLine);
                    errorMsg += exception.Message;
                    MessageBox.Show(errorMsg, "Error: Can't write to XML File",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    // simply return
                    return;
                }
            }
        }

        /*---------------------------------------------------------------------
                           Editing Toolbar Event Handlers
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Paints the select line colour button as the chosen colour.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The PaintEventArgs associated with the graphics
        /// paint</param>
        private void editColourButton_Paint(object sender, PaintEventArgs e)
        {
            // Simply paint the whole button.
            Graphics g = e.Graphics;
            SolidBrush colourBrush = new SolidBrush(m_lineColour);
            Rectangle control = new Rectangle(0, 0, editColourButton.Width,
                                              editColourButton.Height);
            g.FillRectangle(colourBrush, control);
            colourBrush.Dispose();
        }

        /// <summary>
        /// Handles the select line colour button click event. Allows a user to
        /// choose the colour of the lines and labels to draw sub-regions with.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the button click
        /// event</param>
        private void editColourButton_Click(object sender, EventArgs e)
        {
            // Use the colour dialog to let users choose there colour.
            ColorDialog colourChoice = new ColorDialog();
            colourChoice.AllowFullOpen = false;  // Don't allow custom colours.
            colourChoice.SolidColorOnly = true;  // Only solid brushes.

            // If the user chooses a colour, set the colour to the chosen one.
            if (colourChoice.ShowDialog() == DialogResult.OK)
            {
                m_lineColour = colourChoice.Color;
                // Invalidate the button and the canvas so that the new colour
                // is shown.
                editColourButton.Invalidate();
                imageCanvas.Invalidate();
            }
        }

        /// <summary>
        /// Handles the zoom in button click event. Enlarges the image.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the button click
        /// event</param>
        private void editZoomInButton_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Set a maximum zoom level to stop zooms going on
            // forever.
            const int MAX_ZOOM = 200;

            // If the zoom up is in range, change size, otherwise do nothing.
            if (m_zoomLevel + 10 <= MAX_ZOOM)
            {
                // increase the current image display by 10%
                int newWidth = (
                    imageCanvas.Image.Width + (imageCanvas.Image.Width / 10));
                int newHeight = (
                    imageCanvas.Image.Height + (imageCanvas.Image.Height / 10));

                // Change the zoom and zoom level.
                ChangeZoom(newWidth, newHeight);
                m_zoomLevel += 10;
            }
        }

        /// <summary>
        /// Handles the zoom out button click event. Shrinks the image.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the button click
        /// event</param>
        private void editZoomOutButton_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Set a maximum zoom level to stop zooms going on
            // forever.
            const int MIN_ZOOM = 0;

            // If the zoom up is in range, change size, otherwise do nothing.
            if (m_zoomLevel - 10 >= MIN_ZOOM)
            {
                // increase the current image display by 10%
                int newWidth = (
                    imageCanvas.Image.Width - (imageCanvas.Image.Width / 10));
                int newHeight = (
                    imageCanvas.Image.Height - (imageCanvas.Image.Height / 10));

                // Change the zoom and zoom level.
                ChangeZoom(newWidth, newHeight);
                m_zoomLevel -= 10;
            }
        }

        /// <summary>
        /// Handles the add sub-image button click event. Adds a sub-image to
        /// the list of sub images (including to the list views).
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the button click
        /// event</param>
        private void editAddButton_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // When adding a sub-image, we need to create a new SubImage and
            // add it to the list of sub-images. We also need to create a new
            // ListViewItem and add it to the ListView.

            // Create the ListViews first. We will give the new sub-image a
            // default identifier of "new sub image1". However, if "new sub
            // image1" already exists, it will be named "new sub image2", or
            // until a number that doesn't already exist can be found. 
            string newSubImageId = "new sub image1";
            int concatNumber = 1;
            bool isAdded = false;

            while (!isAdded)
            {
                if (imageSubRegions.FindItemWithText(newSubImageId) == null)
                {
                    imageSubRegions.Items.Add(newSubImageId);
                    toggleSubRegions.Items.Add(newSubImageId);
                    isAdded = true;
                }
                else
                {
                    int newConcat = concatNumber + 1;
                    newSubImageId = newSubImageId.Replace(
                        concatNumber.ToString(), newConcat.ToString());
                    concatNumber++;
                }
            }

            // Create and add the SubImage object next.
            SubImage newSubImage = new SubImage();
            newSubImage.m_identifier = newSubImageId;
            m_subImageList.m_items.Add(newSubImage);
        }

        /// <summary>
        /// Handles the remove sub-image button click event. Removes a
        /// sub-image from the list of sub images (including from the list
        /// views).
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the button click
        /// event</param>
        private void editRemoveButton_Click(object sender, EventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // The remove sub-image works by removing the sub-image that is
            // selected in the edit list view. So the first step is to get the
            // Item from the list view that is selected. 
            // We can use the SelectedItems property for this. As the ListView
            // object has it's MultiSelect property set to false, the selected
            // item will be the only item in this property. If nothing is
            // selected however, this property will be empty. Checking this will
            // allow determintation of whether anything is to be deleted or not.
            // If there is no delete to take place, simply return. 
            if (imageSubRegions.SelectedItems.Count == 0)
                return;
            ListViewItem selected = imageSubRegions.SelectedItems[0];

            // Remove the selected sub image from the sub-image list.
            m_subImageList.Remove(selected.Text);

            // Remove from the ListViews. To remove from the toggle ListView,
            // we need to find the item in the listview, then remove it.
            imageSubRegions.Items.Remove(selected);
            ListViewItem found = toggleSubRegions.FindItemWithText(selected.Text);
            toggleSubRegions.Items.Remove(found);
        }

        /*---------------------------------------------------------------------
                           Image Canvas Event Handlers
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Handles the actions for a mouse button being pressed on the image
        /// canvas. There are multiple stages to the Mouse Down event. They
        /// are:
        /// 1. Set the mouse start location to the location when the mouse
        ///    is pressed. 
        /// 2. Handle the editing state.
        /// 3. Set the panning start location.
        /// 
        /// The first stage is critical for a variety of reasons. Some
        /// states use this information for their needs.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The MouseEventArgs associated with the mouse down
        /// event</param>
        private void imageCanvas_MouseDown(object sender, MouseEventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Stage 1, set the mouse starting point.
            if (e.Button == MouseButtons.Left)
            {
                // Set the mouse starting point.
                m_mouseStart = e.Location;
            }

            // Stage 2. Handle the editing state. Remove the region for the
            // selected item. Then simply call the MouseMove delegate with the
            // current edit state.
            if ((e.Button == MouseButtons.Left ||
                 e.Button == MouseButtons.Right) &&
                imageSubRegions.SelectedItems.Count > 0)
            {
                // Clear the Selected sub-image information as it is going to
                // be re-written.
                string selectedListViewItem = (
                    imageSubRegions.SelectedItems[0].Text);
                SubImage selectedSubImage = m_subImageList.Find(
                    selectedListViewItem);
                selectedSubImage.m_location = new Point(
                    (int)(e.Location.X * m_xScale),
                    (int)(e.Location.Y * m_yScale));
                if (selectedSubImage.m_region != null)
                {
                    selectedSubImage.m_region.Clear();
                }

                // Call the Mouse Down event
                string editState = editStateToggleBar.GetCheckedButtonName();
                if (m_mouseDownDelegate.MouseDownTriggered(imageSubRegions,
                                                           m_subImageList,
                                                           e,
                                                           m_mouseStart,
                                                           m_xScale,
                                                           m_yScale,
                                                           editState))
                    imageCanvas.Invalidate();
            }

            // Stage 3. Set the mouse panning start point.
            if (e.Button == MouseButtons.Middle)
            {
                m_middlePressed = true;
                m_panStart = e.Location;
            }
        }

        /// <summary>
        /// Handles the actions performed by moving the mouse over the image
        /// canvas. There are multiple stages to the Mouse Move event.
        /// They are: 
        /// 1. Restrict the mouse to only the main image area.
        /// 2. Get the X and Y Pixel (and display in text boxes).
        /// 3. Handle the editing state.
        /// 4. Handle the panning of the image.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The MouseEventArgs associated with the mouse move
        /// event</param>
        private void imageCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Stage 1. Restrict Mouse to image area. This is so boundaries
            // where areas of the image don't exist can't be defined. As the
            // cursor works in screen co-ordinates (i.e. position measured from
            // the top-left of the screen), changing the cursor requires
            // working with screen co-ordinates. Use the PointToScreen method
            // to convert a point co-ordinate to a screen co-ordinate.

            // Set the cursor position to the edge of the image.
            if (e.Location.X < 0)
                Cursor.Position = new Point(
                    this.RectangleToScreen(imageCanvas.Bounds).Left,
                    Cursor.Position.Y);
            else if (e.Location.X > imageCanvas.Image.Width)
                Cursor.Position = new Point(
                    this.RectangleToScreen(imageCanvas.Bounds).Right,
                    Cursor.Position.Y);

            // On the Y-axis, the main menu and toolbars are sitting on top
            // of the imageCanvas and SplitContainer1.Panel1. The
            // imageCanvas.Top location is therefore sitting underneath them.
            // As such, to restrict the mouse only to the visble image area
            // (visible actually starts from bottom of the editing toolbar),
            // the main menu and toolbar height need to be added to to move the
            // mouse on the y-axis. 
            if (e.Location.Y < 0)
            {
                int menuToolbarTotalHeight = (
                    mainMenu.Height + editingToolbar.Height);
                Cursor.Position = new Point(
                    Cursor.Position.X,
                    this.RectangleToScreen(imageCanvas.Bounds).Top +
                    menuToolbarTotalHeight);
            }
            else if (e.Location.Y > imageCanvas.Image.Height)
            {
                int menuToolbarTotalHeight = (
                    mainMenu.Height + editingToolbar.Height);
                Cursor.Position = new Point(
                    Cursor.Position.X,
                    this.RectangleToScreen(imageCanvas.Bounds).Bottom +
                    menuToolbarTotalHeight);
            }

            // Stage 2. Calculate x and y pixel. This is simply the mouse
            // location scaled to the image.
            int xPixel = (int)(e.Location.X * m_xScale);
            int yPixel = (int)(e.Location.Y * m_yScale);

            // Display the x and y pixels in the text display
            editMouseXTextBox.Text = xPixel.ToString();
            editMouseYTextBox.Text = yPixel.ToString();

            // Stage 3. Handle the editing state. Simply call the MouseMove
            // delegate with the current edit state.
            if ((e.Button == MouseButtons.Left ||
                 e.Button == MouseButtons.Right) &&
                imageSubRegions.SelectedItems.Count > 0)
            {
                string editState = editStateToggleBar.GetCheckedButtonName();
                if (m_mouseMoveDelegate.MouseMoveTriggered(imageSubRegions,
                                                           m_subImageList,
                                                           e,
                                                           m_mouseStart,
                                                           m_xScale,
                                                           m_yScale,
                                                           editState))
                    imageCanvas.Invalidate();
            }

            // Stage 4. Handle panning. Panning is done when the right mouse
            // button is down.
            if (m_middlePressed)
            {
                // We are panning. Panning is done by moving the scrollbar
                // sliders to change the position of the visible area.

                // Calculate the distance and direction of the
                // mouse move.
                int xMove = e.Location.X - m_panStart.X;
                int yMove = e.Location.Y - m_panStart.Y;

                // Calculate the proposed location of the scroll bars.
                int proposedMoveX = (
                    splitContainer1.Panel1.HorizontalScroll.Value + xMove);
                int proposedMoveY = (
                    splitContainer1.Panel1.VerticalScroll.Value + yMove);

                // If the move is within the bounds, move the scrollbar.
                if ((proposedMoveX >=
                        splitContainer1.Panel1.HorizontalScroll.Minimum) &&
                    (proposedMoveX <=
                        splitContainer1.Panel1.HorizontalScroll.Maximum))
                {
                    splitContainer1.Panel1.HorizontalScroll.Value = (
                        proposedMoveX);

                    // Change the cursor.
                    imageCanvas.Cursor = Cursors.SizeAll;
                }
                if ((proposedMoveY >=
                        splitContainer1.Panel1.VerticalScroll.Minimum) &&
                    (proposedMoveY <=
                        splitContainer1.Panel1.VerticalScroll.Maximum))
                {
                    splitContainer1.Panel1.VerticalScroll.Value = (
                        proposedMoveY);
                }

                // Reset the panning start point.
                m_panStart = e.Location;
            }
        }

        /// <summary>
        /// Handles the actions for a mouse button being let go while over the
        /// image canvas. There are multiple stages to the Mouse Up event. They
        /// are:
        /// 1. Handle the editing state.
        /// 2. Change the cursor back to the default cursor.
        /// 3. Handle panning
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The MouseEventArgs associated with the mouse up
        /// event</param>
        private void imageCanvas_MouseUp(object sender, MouseEventArgs e)
        {
            
            // If no Image, do nothing.
            if (!m_isImageLoaded)
                return;

            // Stage 1. Handle the editing state. Simply call the MouseMove
            // delegate with the current edit state.
            if ((e.Button == MouseButtons.Left ||
                 e.Button == MouseButtons.Right) &&
                imageSubRegions.SelectedItems.Count > 0)
            {
                string editState = editStateToggleBar.GetCheckedButtonName();
                if (m_mouseUpDelegate.MouseUpTriggered(imageSubRegions,
                                                       m_subImageList,
                                                       e,
                                                       m_mouseStart,
                                                       m_xScale,
                                                       m_yScale,
                                                       editState))
                    imageCanvas.Invalidate();
            }

            // Stage 3: Handle panning. Set the cursor back to the default. Set
            // the middle pressed trigger to false.
            if (m_middlePressed)
            {
                m_middlePressed = false;
                imageCanvas.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Handles the image canvas paint event. Paints all the selected
        /// sub-images, as well as the currently editing boundary.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The PaintEventArgs associated with the graphics
        /// paint</param>
        private void imageCanvas_Paint(object sender, PaintEventArgs e)
        {
            // Firstly, Get the Pen and brush of the chosen colour.
            Graphics g = e.Graphics;
            Pen linePen = new Pen(m_lineColour, 3.0f);
            SolidBrush labelBrush = new SolidBrush(m_lineColour);

            // Draw any of the edit boundaries if they are currently being
            // drawn.
            if (DrawRectangle.isDrawing)
            {
                g.DrawRectangle(linePen, DrawRectangle.drawnRectangle);
            }
            if (DrawPolygon.isDrawing)
            {
                Point[] lines = ConvertPixelToScreen(
                        DrawPolygon.drawnPolygon);
                if (DrawPolygon.drawnPolygon.Count >= 2)
                {
                    g.DrawLines(linePen, lines);
                    g.DrawLine(linePen, lines[lines.Count()-1],
                        imageCanvas.PointToClient(Cursor.Position));
                }
                else if (DrawPolygon.drawnPolygon.Count == 1)
                {
                    g.DrawLine(linePen, lines[0],
                        imageCanvas.PointToClient(Cursor.Position));
                }
                imageCanvas.Invalidate();
            }
            if (DrawFreeForm.isDrawing)
            {
                if (DrawFreeForm.drawnFreeForm.Count >= 2)
                    g.DrawLines(linePen,
                        ConvertPixelToScreen(DrawFreeForm.drawnFreeForm));
            }

            // Go through the sub-images and draw any boundaries and labels
            // that are checked.
            foreach (ListViewItem checkedSubImage in
                toggleSubRegions.CheckedItems)
            {
                SubImage sub = m_subImageList.Find(checkedSubImage.Text);
                if (sub.m_region != null)
                {
                    if (sub.m_region.Count > 0)
                    // As the m_region stores it in pixels, we need to scale
                    // the pixel back to screen.
                    {
                        Point[] polygon = ConvertPixelToScreen(sub.m_region);
                        g.DrawPolygon(linePen, polygon);
                        // Position the label just to the top left of the
                        // sub-image. If it won't fit there, then set it to
                        // the bottom left.
                        float xPos = (float)polygon.Min(point => point.X);
                        float yPos = (float)polygon.Min(point => point.Y) - 20.0f;
                        if (yPos <= 10.0f)
                            yPos = (float)polygon.Max(point => point.Y);
                        g.DrawString(sub.m_identifier,
                                     m_labelFont,
                                     labelBrush,
                                     xPos, yPos);
                    }
                }
            }

            // Dispose the Pen and brush
            linePen.Dispose();
            labelBrush.Dispose();
        }

        /// <summary>
        /// Handles the drag enter event (on the split container for which the
        /// image canvas is placed in). Determines whether the file dragged
        /// onto the image canvas is a valid image file to drop and load.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The DragEventArgs associated with the drag event
        /// </param>
        private void splitContainer1_Panel1_DragEnter(object sender,
                                                      DragEventArgs e)
        {
            // Use the GetFileName method to handle the file logic.
            string filename;

            // If it's valid, allow it be be dropped. Otherwise, stop the file
            // from being dropped.
            if (GetFileName(out filename, e))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the Drop event (on the split container for which the
        /// image canvas is placed in). Loads the image file into the
        /// application (if it's a valid image file).
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The DragEventArgs associated with the drop event
        /// </param>
        private void splitContainer1_Panel1_DragDrop(object sender, DragEventArgs e)
        {
            // Use the GetFileName method to handle the file logic.
            string filename;

            // If valid, load the image.
            if (GetFileName(out filename, e))
            {
                LoadImage(filename);
            }
        }

        /*---------------------------------------------------------------------
                         Sub-Image List View Event Handlers
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Handles the ListView label (text) before edit event. Stores the
        /// current identifier before it is edited.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The LabelEditEventArgs associated with the label
        /// editing event</param>
        private void imageSubRegions_BeforeLabelEdit(object sender,
                                                     LabelEditEventArgs e)
        {
            // Get the selected Item, then set the current identifier.
            ListViewItem selected = imageSubRegions.SelectedItems[0];
            m_currentIdentifier = selected.Text;
        }
        
        /// <summary>
        /// Handles the ListView label (text) after edit event. Checks the
        /// user's edited identifier is a unique identifier and changes the
        /// identifier of the SubImage if it is unique.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The LabelEditEventArgs associated with the label
        /// editing event</param>
        private void imageSubRegions_AfterLabelEdit(object sender,
                                                    LabelEditEventArgs e)
        {
            // If no edit was actually made, simply return.
            if (e.Label == null)
                return;

            // Sub-Image identifiers MUST be unique. The first thing is to
            // check is that the new identifier does not already exist.
            if (imageSubRegions.FindItemWithText(e.Label) != null)
            {
                // Already exists. Cancel the edit and print an error message.
                e.CancelEdit = true;
                string errorMsg = "Sub-Image identifiers must be unique. ";
                errorMsg += "Please choose a identifier that does not already";
                errorMsg += " exist.";
                MessageBox.Show(errorMsg,
                    "Error: Sub-Image identifier not unique",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                // simply return
                return;
            }

            // If valid, allow the edit to go ahead.
            e.CancelEdit = false;
            // Change the identifier, both for the ListItems and the
            // corresponding SubImage object.
            ListViewItem selectedToggle = toggleSubRegions.FindItemWithText(
                imageSubRegions.Items[e.Item].Text);
            selectedToggle.Text = e.Label;
            imageSubRegions.Items[e.Item].Text = e.Label;
            SubImage editedSubImage = m_subImageList.Find(m_currentIdentifier);
            editedSubImage.m_identifier = e.Label;
            imageCanvas.Invalidate();
        }

        /// <summary>
        /// Handles the ListView item (sub-image) check state changing event.
        /// When the user turns on or off a Sub-Image region to view, the image
        /// canvas is re-drawn.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The ItemCheckEventArgs associated with the item
        /// check event</param>
        private void toggleSubRegions_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            imageCanvas.Invalidate();
        }

        /// <summary>
        /// Handles the ListView item (sub-image) selection changed event.
        /// Sets the checked state of the sub-image to true to draw the item.
        /// </summary>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The EventArgs associated with the item
        /// selection event</param>
        private void imageSubRegions_SelectedIndexChanged(object sender,
                                                          EventArgs e)
        {
            // Get the newly selected item, find it in the toggle list, then
            // set it's checked state to true.
            if (imageSubRegions.SelectedItems.Count > 0)
            {
                ListViewItem selected = imageSubRegions.SelectedItems[0];
                ListViewItem toggleSelected = toggleSubRegions.FindItemWithText(
                                                   selected.Text);

                // Set it's checked state to true.
                toggleSelected.Checked = true;
            }
        }

        /*---------------------------------------------------------------------
                                  Helper Methods
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Loads the image with the given filepath into the application.
        /// </summary>
        /// <param name="filepath">File path to the image to load</param>
        private void LoadImage(string filepath)
        {
            m_originalImage = new Bitmap(filepath);
            m_isImageLoaded = true;
            m_imageFilePath = filepath;
            imageCanvas.Image = m_originalImage;
            CalculateScaling();
            m_zoomLevel = 100;
        }

        /// <summary>
        /// Calculates the original image to display size scaling factors.
        /// </summary>
        private void CalculateScaling()
        {
            if (!m_isImageLoaded)
                return;
            
            m_xScale = ((float)m_originalImage.Width /
                        (float)imageCanvas.Image.Width);

            m_yScale = ((float)m_originalImage.Height /
                        (float)imageCanvas.Image.Height);
        }

        /// <summary>
        /// Populates the sub-image list views with the sub images.
        /// </summary>
        void PopulateSubImageList()
        {
            // We need to use the SubImage objects to create List View objects,
            // using the identifier as the name to display.

            // First, clear the ListViews if already populated.
            imageSubRegions.Items.Clear();
            toggleSubRegions.Items.Clear();

            // Go through each of the subImages and use it's identifier to
            // create a ListView item. For the toggle one, set it's checked to
            // true.
            foreach (SubImage subimage in m_subImageList.m_items)
            {
                ListViewItem editItem = new ListViewItem(subimage.m_identifier);
                imageSubRegions.Items.Add(editItem);
                ListViewItem toggleItem = new ListViewItem(subimage.m_identifier);
                toggleSubRegions.Items.Add(toggleItem);
                toggleItem.Checked = true;
            }
        }

        /// <summary>
        /// Changes the size (zoom) of the image.
        /// </summary>
        /// <param name="newWidth">The new width to set the image to</param>
        /// <param name="newHeight">The new height to set the image to</param>
        void ChangeZoom(int newWidth, int newHeight)
        {
            // Create a temporary bitmap from the original image with the new
            // size.  This will be used as the picturebox image. Keeping the
            // original will allow scaling from the original image to the
            // visible image possible.
            Bitmap temp = new Bitmap(m_originalImage, newWidth, newHeight);

            // Set the resolution to the original image to keep the image
            // quality as good as possible.
            temp.SetResolution(m_originalImage.HorizontalResolution,
                               m_originalImage.VerticalResolution);

            // Set the picture box image. and calculate scale.
            imageCanvas.Image = temp;
            CalculateScaling();
            imageCanvas.Invalidate();
        }

        /// <summary>
        /// Builds the file extension filter string to use in dialogs.
        /// </summary>
        /// <returns>A string of all the valid image extensions to use as a
        /// filter in a open or save file dialog box</returns>
        private string BuildImageFilterString()
        {
            /*
             * To specify a filter, the first item before the | is the text 
             * that gets displayed in the dialog, whereas the bit after the |
             * is the wildcard filter. Another | separates another option.
             */
            // This filter is going to show all valid extensions as one option.
            // the format for this is:
            // "Image Files (...extensions)|*.extension1; *.extension2..."
            string displayPart = "";
            string filterPart = "";

            // Go through each of the valid extensions and concatenate it to
            // the two parts.
            foreach (string extension in validExtensions)
            {
                displayPart += string.Format("*.{0}, ", extension);
                filterPart += string.Format("*.{0};", extension);
            }
            // Remove the trailing separator characters.
            char[] charsToTrim = { ' ', ',', ';' };
            displayPart = displayPart.TrimEnd(charsToTrim);
            filterPart = filterPart.TrimEnd(charsToTrim);

            // Make the final string.
            string filterString = string.Format(
                "Image Files({0})|{1}", displayPart, filterPart);

            return filterString;
        }

        /// <summary>
        /// Finds the file name of the data object dragged and dropped into the
        /// application. Also determines whether the data object is a valid file
        /// for an image. The method returns true if it is, returns false
        /// otherwise.
        /// </summary>
        /// <param name="filename">The file path to the file being dragged
        /// </param>
        /// <param name="e">The DragEventArgs associated with the drag event
        /// </param>
        /// <returns>true if a valid image file, false otherwise</returns>
        private bool GetFileName(out string filename, DragEventArgs e)
        {
            // Use the "FileName" format of the data object to get the full
            // file path of the file being dropped. The FileName format
            // contains the filename in a string array. Cast it into this type.
            string[] dropData = (string[])e.Data.GetData("FileName");
            filename = dropData[0];

            // Get the file extension of the file.
            string fileExtension = filename.Split('.')[1];

            // Check if the file extension is valid and return the correct
            // result.
            if (ImageSplitForm.validExtensions.Contains(fileExtension))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Converts the pixel co-ordinates in the Point list to an array of
        /// Points whose co-ordinates have been converted to screen
        /// co-ordinates.
        /// </summary>
        /// <param name="pointList">The List of Points in pixel co-ordinates
        /// to convert to screen co-ordinates</param>
        /// <returns>A Point[] of the converted co-ordinates</returns>
        private Point[] ConvertPixelToScreen(List<Point> pointList)
        {
            Point[] screenCoords = new Point[pointList.Count];
            int arrayCount = 0;

            foreach(Point pixelPoint in pointList)
            {
                screenCoords[arrayCount++] = new Point(
                    (int)(pixelPoint.X / m_xScale),
                    (int)(pixelPoint.Y / m_yScale));
            }

            return screenCoords;
        }
    }
}
