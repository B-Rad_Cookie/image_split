﻿namespace image_split
{
    partial class ImageSplitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageSplitForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenuList = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editingToolbar = new System.Windows.Forms.ToolStrip();
            this.editAddButton = new System.Windows.Forms.ToolStripButton();
            this.editRemoveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.editColourButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.editZoomInButton = new System.Windows.Forms.ToolStripButton();
            this.editZoomOutButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.editMouseXTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.editMouseYTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.imageCanvas = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.imageSubRegions = new System.Windows.Forms.ListView();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.toggleSubRegions = new System.Windows.Forms.ListView();
            this.editStateToggleBar = new image_split.ToggleToolStrip();
            this.editRectangleButton = new System.Windows.Forms.ToolStripButton();
            this.editPolygonButton = new System.Windows.Forms.ToolStripButton();
            this.editFreeFormButton = new System.Windows.Forms.ToolStripButton();
            this.mainMenu.SuspendLayout();
            this.editingToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCanvas)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.editStateToggleBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuList});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(723, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenuList
            // 
            this.fileMenuList.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageMenuItem,
            this.openXmlMenuItem,
            this.saveMenuItem,
            this.saveAsMenuItem});
            this.fileMenuList.Name = "fileMenuList";
            this.fileMenuList.Size = new System.Drawing.Size(37, 20);
            this.fileMenuList.Text = "File";
            // 
            // openImageMenuItem
            // 
            this.openImageMenuItem.Name = "openImageMenuItem";
            this.openImageMenuItem.Size = new System.Drawing.Size(188, 22);
            this.openImageMenuItem.Text = "&Open Image";
            this.openImageMenuItem.Click += new System.EventHandler(this.openImageMenuItem_Click);
            // 
            // openXmlMenuItem
            // 
            this.openXmlMenuItem.Name = "openXmlMenuItem";
            this.openXmlMenuItem.Size = new System.Drawing.Size(188, 22);
            this.openXmlMenuItem.Text = "&Load Sub-Image XML";
            this.openXmlMenuItem.Click += new System.EventHandler(this.openXmlMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(188, 22);
            this.saveMenuItem.Text = "&Save";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // saveAsMenuItem
            // 
            this.saveAsMenuItem.Name = "saveAsMenuItem";
            this.saveAsMenuItem.Size = new System.Drawing.Size(188, 22);
            this.saveAsMenuItem.Text = "S&ave As";
            this.saveAsMenuItem.Click += new System.EventHandler(this.saveAsMenuItem_Click);
            // 
            // editingToolbar
            // 
            this.editingToolbar.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.editingToolbar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.editingToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editAddButton,
            this.editRemoveButton,
            this.toolStripSeparator1,
            this.editColourButton,
            this.toolStripSeparator2,
            this.editZoomInButton,
            this.editZoomOutButton,
            this.toolStripSeparator5,
            this.toolStripLabel1,
            this.editMouseXTextBox,
            this.toolStripLabel2,
            this.editMouseYTextBox});
            this.editingToolbar.Location = new System.Drawing.Point(0, 24);
            this.editingToolbar.Name = "editingToolbar";
            this.editingToolbar.Size = new System.Drawing.Size(723, 25);
            this.editingToolbar.TabIndex = 1;
            this.editingToolbar.Text = "Editing Toolbar";
            // 
            // editAddButton
            // 
            this.editAddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editAddButton.Image = global::image_split.Properties.Resources.edit_add;
            this.editAddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editAddButton.Name = "editAddButton";
            this.editAddButton.Size = new System.Drawing.Size(23, 22);
            this.editAddButton.Text = "toolStripButton2";
            this.editAddButton.ToolTipText = "Add Sub-Image";
            this.editAddButton.Click += new System.EventHandler(this.editAddButton_Click);
            // 
            // editRemoveButton
            // 
            this.editRemoveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editRemoveButton.Image = global::image_split.Properties.Resources.edit_remove;
            this.editRemoveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editRemoveButton.Name = "editRemoveButton";
            this.editRemoveButton.Size = new System.Drawing.Size(23, 22);
            this.editRemoveButton.Text = "toolStripButton1";
            this.editRemoveButton.ToolTipText = "Delete Selected Sub-Image";
            this.editRemoveButton.Click += new System.EventHandler(this.editRemoveButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // editColourButton
            // 
            this.editColourButton.CheckOnClick = true;
            this.editColourButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.editColourButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editColourButton.Name = "editColourButton";
            this.editColourButton.Size = new System.Drawing.Size(23, 22);
            this.editColourButton.Text = "toolStripButton3";
            this.editColourButton.ToolTipText = "Select Line Colour";
            this.editColourButton.Click += new System.EventHandler(this.editColourButton_Click);
            this.editColourButton.Paint += new System.Windows.Forms.PaintEventHandler(this.editColourButton_Paint);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // editZoomInButton
            // 
            this.editZoomInButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editZoomInButton.Image = global::image_split.Properties.Resources.zoomin;
            this.editZoomInButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editZoomInButton.Name = "editZoomInButton";
            this.editZoomInButton.Size = new System.Drawing.Size(23, 22);
            this.editZoomInButton.Text = "toolStripButton1";
            this.editZoomInButton.ToolTipText = "Zoom In";
            this.editZoomInButton.Click += new System.EventHandler(this.editZoomInButton_Click);
            // 
            // editZoomOutButton
            // 
            this.editZoomOutButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editZoomOutButton.Image = global::image_split.Properties.Resources.zoom_out;
            this.editZoomOutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editZoomOutButton.Name = "editZoomOutButton";
            this.editZoomOutButton.Size = new System.Drawing.Size(23, 22);
            this.editZoomOutButton.Text = "toolStripButton2";
            this.editZoomOutButton.ToolTipText = "Zoom Out";
            this.editZoomOutButton.Click += new System.EventHandler(this.editZoomOutButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel1.Text = "Pixel X:";
            // 
            // editMouseXTextBox
            // 
            this.editMouseXTextBox.Name = "editMouseXTextBox";
            this.editMouseXTextBox.ReadOnly = true;
            this.editMouseXTextBox.Size = new System.Drawing.Size(100, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel2.Text = "Pixel Y:";
            // 
            // editMouseYTextBox
            // 
            this.editMouseYTextBox.Name = "editMouseYTextBox";
            this.editMouseYTextBox.ReadOnly = true;
            this.editMouseYTextBox.Size = new System.Drawing.Size(100, 25);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitContainer1.Panel1.Controls.Add(this.imageCanvas);
            this.splitContainer1.Panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.splitContainer1_Panel1_DragDrop);
            this.splitContainer1.Panel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.splitContainer1_Panel1_DragEnter);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(723, 412);
            this.splitContainer1.SplitterDistance = 529;
            this.splitContainer1.TabIndex = 2;
            // 
            // imageCanvas
            // 
            this.imageCanvas.Location = new System.Drawing.Point(0, 0);
            this.imageCanvas.Name = "imageCanvas";
            this.imageCanvas.Size = new System.Drawing.Size(529, 412);
            this.imageCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imageCanvas.TabIndex = 0;
            this.imageCanvas.TabStop = false;
            this.imageCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.imageCanvas_Paint);
            this.imageCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageCanvas_MouseDown);
            this.imageCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageCanvas_MouseMove);
            this.imageCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageCanvas_MouseUp);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.splitContainer2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(190, 412);
            this.panel1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer2.Size = new System.Drawing.Size(190, 412);
            this.splitContainer2.SplitterDistance = 206;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.imageSubRegions);
            this.splitContainer3.Size = new System.Drawing.Size(190, 206);
            this.splitContainer3.SplitterDistance = 25;
            this.splitContainer3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Sub-Image To Edit";
            // 
            // imageSubRegions
            // 
            this.imageSubRegions.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.imageSubRegions.BackColor = System.Drawing.SystemColors.Window;
            this.imageSubRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageSubRegions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imageSubRegions.HideSelection = false;
            this.imageSubRegions.LabelEdit = true;
            this.imageSubRegions.LabelWrap = false;
            this.imageSubRegions.Location = new System.Drawing.Point(0, 0);
            this.imageSubRegions.MultiSelect = false;
            this.imageSubRegions.Name = "imageSubRegions";
            this.imageSubRegions.Size = new System.Drawing.Size(190, 177);
            this.imageSubRegions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.imageSubRegions.TabIndex = 0;
            this.imageSubRegions.UseCompatibleStateImageBehavior = false;
            this.imageSubRegions.View = System.Windows.Forms.View.List;
            this.imageSubRegions.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.imageSubRegions_AfterLabelEdit);
            this.imageSubRegions.BeforeLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.imageSubRegions_BeforeLabelEdit);
            this.imageSubRegions.SelectedIndexChanged += new System.EventHandler(this.imageSubRegions_SelectedIndexChanged);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.toggleSubRegions);
            this.splitContainer4.Size = new System.Drawing.Size(190, 202);
            this.splitContainer4.SplitterDistance = 25;
            this.splitContainer4.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Toggle Sub-Image To Display";
            // 
            // toggleSubRegions
            // 
            this.toggleSubRegions.CheckBoxes = true;
            this.toggleSubRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toggleSubRegions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.toggleSubRegions.LabelWrap = false;
            this.toggleSubRegions.Location = new System.Drawing.Point(0, 0);
            this.toggleSubRegions.MultiSelect = false;
            this.toggleSubRegions.Name = "toggleSubRegions";
            this.toggleSubRegions.Size = new System.Drawing.Size(190, 173);
            this.toggleSubRegions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.toggleSubRegions.TabIndex = 0;
            this.toggleSubRegions.UseCompatibleStateImageBehavior = false;
            this.toggleSubRegions.View = System.Windows.Forms.View.List;
            this.toggleSubRegions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.toggleSubRegions_ItemCheck);
            // 
            // editStateToggleBar
            // 
            this.editStateToggleBar.BackColor = System.Drawing.SystemColors.ControlLight;
            this.editStateToggleBar.Dock = System.Windows.Forms.DockStyle.None;
            this.editStateToggleBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editRectangleButton,
            this.editPolygonButton,
            this.editFreeFormButton});
            this.editStateToggleBar.Location = new System.Drawing.Point(435, 24);
            this.editStateToggleBar.Name = "editStateToggleBar";
            this.editStateToggleBar.Size = new System.Drawing.Size(81, 25);
            this.editStateToggleBar.TabIndex = 3;
            this.editStateToggleBar.Text = "toggleToolStrip1";
            // 
            // editRectangleButton
            // 
            this.editRectangleButton.Checked = true;
            this.editRectangleButton.CheckOnClick = true;
            this.editRectangleButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.editRectangleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editRectangleButton.Image = global::image_split.Properties.Resources.draw_square;
            this.editRectangleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editRectangleButton.Name = "editRectangleButton";
            this.editRectangleButton.Size = new System.Drawing.Size(23, 22);
            this.editRectangleButton.Text = "toolStripButton1";
            this.editRectangleButton.ToolTipText = "Draw Rectangle Sub-Image";
            // 
            // editPolygonButton
            // 
            this.editPolygonButton.CheckOnClick = true;
            this.editPolygonButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editPolygonButton.Image = global::image_split.Properties.Resources.draw_polygon;
            this.editPolygonButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editPolygonButton.Name = "editPolygonButton";
            this.editPolygonButton.Size = new System.Drawing.Size(23, 22);
            this.editPolygonButton.Text = "toolStripButton2";
            this.editPolygonButton.ToolTipText = "Draw Polygon Sub-Image";
            // 
            // editFreeFormButton
            // 
            this.editFreeFormButton.CheckOnClick = true;
            this.editFreeFormButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editFreeFormButton.Image = global::image_split.Properties.Resources.draw_free;
            this.editFreeFormButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editFreeFormButton.Name = "editFreeFormButton";
            this.editFreeFormButton.Size = new System.Drawing.Size(23, 22);
            this.editFreeFormButton.Text = "toolStripButton3";
            this.editFreeFormButton.ToolTipText = "Draw Free-Form Sub-Image";
            // 
            // ImageSplitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 461);
            this.Controls.Add(this.editStateToggleBar);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.editingToolbar);
            this.Controls.Add(this.mainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Name = "ImageSplitForm";
            this.Text = "Image Split";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.editingToolbar.ResumeLayout(false);
            this.editingToolbar.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCanvas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.editStateToggleBar.ResumeLayout(false);
            this.editStateToggleBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenuList;
        private System.Windows.Forms.ToolStripMenuItem openImageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openXmlMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsMenuItem;
        private System.Windows.Forms.ToolStrip editingToolbar;
        private System.Windows.Forms.ToolStripButton editRemoveButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripTextBox editMouseXTextBox;
        private System.Windows.Forms.ToolStripTextBox editMouseYTextBox;
        private System.Windows.Forms.ToolStripButton editAddButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton editColourButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.PictureBox imageCanvas;
        private System.Windows.Forms.ToolStripButton editZoomInButton;
        private System.Windows.Forms.ToolStripButton editZoomOutButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private ToggleToolStrip editStateToggleBar;
        private System.Windows.Forms.ToolStripButton editRectangleButton;
        private System.Windows.Forms.ToolStripButton editPolygonButton;
        private System.Windows.Forms.ToolStripButton editFreeFormButton;
        private System.Windows.Forms.ListView imageSubRegions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView toggleSubRegions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
    }
}

