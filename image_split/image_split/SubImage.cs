﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace image_split
{
    /// <summary>
    /// A list of SubImages. This class is used to store a list of SubImage
    /// objects, as well as serialise and deserialise them to xml.
    /// </summary>
    public class SubImageList
    {
        /*---------------------------------------------------------------------
                                  Public Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The list of subimage objects
        /// </summary>
        [XmlElement("Sub_Image")]
        public List<SubImage> m_items;

        /// <summary>
        /// The file path to the image.
        /// </summary>
        [XmlElement("Image_Filepath")]
        public string m_imageFilePath;

        /*---------------------------------------------------------------------
                                   Constructors
        ---------------------------------------------------------------------*/
        /// <summary>
        /// Empty Constructor. Initialise the list.
        /// </summary>
        public SubImageList() { m_items = new List<SubImage>(); }

        /*---------------------------------------------------------------------
                                  Public Methods
        ---------------------------------------------------------------------*/

        /// <summary>
        /// Serialises the sub-image list to the given xml file path.
        /// </summary>
        /// <param name="xmlFilePath">File path to the .xml file to save the
        /// object information to</param>
        /// <param name="imageFilePath">File path to the image for which the
        /// sub-images are linked to</param>
        /// <exception cref="System.IO.IOException">Thrown if the file path is
        /// not a valid .xml file</exception>
        public void Serialise(string xmlFilePath, string imageFilePath)
        {
            // Set the image file path
            if(imageFilePath != m_imageFilePath)
            {
                m_imageFilePath = imageFilePath;
            }

            // If the string is not .xml, raise an exception
            if (!xmlFilePath.EndsWith(".xml"))
            {
                throw new IOException(
                    String.Format("{0} is not a valid xml path", xmlFilePath));
            }

            // Setup the serialisation, do the serialisation, then close the
            // writer stream.
            XmlSerializer subImageSerialise = new XmlSerializer(
                typeof(SubImageList));
            StreamWriter xmlFileWriter = new StreamWriter(xmlFilePath);
            subImageSerialise.Serialize(xmlFileWriter, this);
            xmlFileWriter.Close();
        }

        /// <summary>
        /// Reads sub-image list information from the given xml file path and
        /// creates a SubImageList object. 
        /// </summary>
        /// <remarks>
        /// Deserialisation will only be successful if the given .xml file has
        /// xml tree structure expected for a SubImageList. 
        /// </remarks>
        /// <param name="xmlFilePath">File path to the .xml file to read
        /// SubImageList information from</param>
        /// <returns>A SubImageList object initialised with the read
        /// information
        /// </returns>
        /// <exception cref="System.IO.IOException">Thrown if the file path is
        /// not a valid .xml file</exception>
        public static SubImageList Deserialise(string xmlFilePath)
        {
            // If the string is not .xml, raise an exception
            if (!xmlFilePath.EndsWith(".xml"))
            {
                throw new IOException(
                    String.Format("{0} is not a valid xml path", xmlFilePath));
            }

            // Setup the deserialisation
            XmlSerializer subImageDeserialise = new XmlSerializer(
                typeof(SubImageList));
            StreamReader xmlFileReader = new StreamReader(xmlFilePath);

            // Deserialise, close stream, then return new subimage list.
            SubImageList s = subImageDeserialise.Deserialize(xmlFileReader) as SubImageList;
            xmlFileReader.Close();
            return s;
        }

        ///<summary>
        /// Returns the SubImage item with the given identifier. 
        /// </summary>
        /// <param name="identifier">The identifier of the SubImage to find.
        /// </param>
        /// <returns>
        /// The SubImage with the given identifier. null if there is no
        /// SubImage with that identifier in the list.
        /// </returns>
       public SubImage Find(string identifier)
        {
            // Find the sub image by iterting through the new list until it is
            // found.
            SubImage returnSubImage = null;
            foreach (SubImage subimage in m_items)
            {
                if (subimage.m_identifier == identifier)
                {
                    returnSubImage = subimage;
                    return returnSubImage;
                }
            }
            return returnSubImage;
        }

       ///<summary>
       /// Removes the SubImage item with the given identifier. 
       /// </summary>
       /// <param name="identifier">The identifier of the SubImage to remove.
       /// </param>
       public void Remove(string identifier)
       {
           // Find the sub image by iterting through the new list until it is
           // found.
           SubImage removeSubImage = null;
           foreach (SubImage subimage in m_items)
           {
               if (subimage.m_identifier == identifier)
               {
                   removeSubImage = subimage;
                   break;
               }
           }

           // Remove the subImage.
           if (removeSubImage != null)
           {
               m_items.Remove(removeSubImage);
           }
       }
    }


    /// <summary>
    /// A struct of the information required for a sub-image.
    /// </summary>
    public class SubImage
    {
        /*---------------------------------------------------------------------
                                   Public Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The identifier for a sub-image.
        /// </summary>
        [XmlElement("identifier")]
        public string m_identifier;

        /// <summary>
        /// The location of the first vertex.
        /// </summary>
        [XmlElement("location")]
        public Point m_location;

        /// <summary>
        /// The dimensions of the sub-image. The dimension/region will be
        /// stored as a list of points containing each vertex.
        /// </summary>
        [XmlArray("vertices")]
        [XmlArrayItem("point")]
        public List<Point> m_region;

        /*---------------------------------------------------------------------
            Constructors
         --------------------------------------------------------------------*/

        /// <summary>
        /// Empty Constructor. Reutrns a new SubImage object whose values are
        /// initialised to null.
        /// </summary>
        public SubImage()
        {
            // Set (or leave) everything to null
            m_identifier = null;
            m_region = null;
        }

        /// <summary>
        /// Copy Constructor. Returns a new SubImage object whose values are
        /// copied from another SubImage object.
        /// </summary>
        /// <param name="otherSubImage">The SubImage object to copy values
        /// from.</param>
        public SubImage(SubImage otherSubImage)
        {
            // Make sure all the copies are deep copies.
            m_identifier = String.Copy(otherSubImage.m_identifier);
            m_location = new Point(otherSubImage.m_location.X,
                                   otherSubImage.m_location.Y);

            m_region = otherSubImage.m_region.ConvertAll(
                point => new Point(point.X, point.Y));
        }

        /// <summary>
        /// Item Constructor. Returns a new SubImage object with the member
        /// variables assigned to the values in the arguments.
        /// </summary>
        /// <param name="identifier">The identifier for the SubImage</param>
        /// <param name="location">Location of the first vertex</param>
        /// <param name="region">The Point list defining the boundary of the
        /// SubImage</param>
        public SubImage(string identifier,
                        Point location,
                        List<Point> region)
        {
            // make sure all the copies are deep copies.
            m_identifier = String.Copy(identifier);
            m_location = new Point(location.X, location.Y);
            m_region = region.ConvertAll(point => new Point(point.X, point.Y));
        }
    }
}
