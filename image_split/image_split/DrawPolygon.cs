﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    /// <summary>
    /// Contains the handling of drawing a polygon as a boundary of a
    /// sub-image. It handles the one stage required for drawing a
    /// polygon: The MouseDown.
    /// </summary>
    /// <remarks>
    /// As the method in this class is designed to be a delegate subscriber and
    /// this class is representing a editing "state", the purpose of the class
    /// is a utility class to provide functionalities for drawing polygons. As
    /// such, all members required in the drawing of a polygon will be a static
    /// on the class.
    /// </remarks>
    static class DrawPolygon
    {
        /*---------------------------------------------------------------------
                              Public Static members
         --------------------------------------------------------------------*/
        /// <summary>
        /// The Polygon to draw.
        /// </summary>
        public static List<Point> drawnPolygon;

        /// <summary>
        /// Determines whether a new polygon is being drawn.
        /// </summary>
        public static bool isDrawing = false;

        /// <summary>
        /// The MouseDown event handler for drawing a polygon. This handles the
        /// creation of vertices of the polygon at each click, as well as the
        /// closing of the polygon on the double click. This method is designed
        /// to be subscribed to the MouseDownEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public static bool PolygonMouseDownEvent(ListView subImageListView,
                                                 SubImageList subImages,
                                                 MouseEventArgs e,
                                                 Point mouseStart,
                                                 float xScale, float yScale,
                                                 string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the polygon button
            // "editPolygonButton", do nothing.
            if (editState != "editPolygonButton")
            {
                return false;
            }

            // Set the isDrawing to true
            DrawPolygon.isDrawing = true;

            //  If this is a new polygon, reset the drawnPolygon to a new list.
            if (DrawPolygon.drawnPolygon == null)
                DrawPolygon.drawnPolygon = new List<Point>();
 
            // add another point to the polygon.
            Point nextPoint = new Point((int)(e.Location.X * xScale),
                                        (int)(e.Location.Y * yScale));
            DrawPolygon.drawnPolygon.Add(nextPoint);

            // If the mouse was a right click, then we have stopped drawing.
            // If this is the case, save the information to the selected
            // sub-image, set isDrawing to false, and set the drawnPolygon back
            // to null.
            if (e.Button == MouseButtons.Right)
            {
                // Save
                SubImage selected =
                    subImages.Find(subImageListView.SelectedItems[0].Text);
                selected.m_region = drawnPolygon;

                // Set
                DrawPolygon.isDrawing = false;
                DrawPolygon.drawnPolygon = null;
            }

            // redraw required.
            return true;
        }
    }
}
