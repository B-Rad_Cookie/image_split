﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    /// <summary>
    /// Contains a delegate which contains all the methods which process a
    /// MouseDown event. This is to allow a MouseDown event being handled
    /// based on the Editing State (whether it be Select, Rectangle, Polygon
    /// or Free Form).
    /// </summary>
    class MouseDownEvent
    {
        /*---------------------------------------------------------------------
                                  Private Methods
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate for all methods which handle a MouseDown Event
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public delegate bool MouseDown(ListView subImageListView,
                                       SubImageList subImages,
                                       MouseEventArgs e,
                                       Point mouseStart,
                                       float xScale, float yScale,
                                       string editState);

        /// <summary>
        /// Subscribes a method to the MouseDown delegate event.
        /// </summary>
        /// <param name="func">The method to subscribe</param>
        public void Subscribe(MouseDown func)
        {
            mouseDownEvent += func;
        }

        /// <summary>
        /// The method to call to trigger the MouseDown delegate event.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        public bool MouseDownTriggered(ListView subImageListView,
                                       SubImageList subImages,
                                       MouseEventArgs e,
                                       Point mouseStart,
                                       float xScale, float yScale,
                                       string editState)
        {
            // We need to read each of the return results, cause if one
            // evaluates to true, then the function should return true.
            // Create a list to store all the results.
            List<bool> returnResults = new List<bool>();

            // Iterate through each method and store it's result in the list.
            // Do this by getting the delegate's InvocationList.
            foreach (MouseDown down in mouseDownEvent.GetInvocationList())
            {
                returnResults.Add(down(subImageListView, subImages, e,
                                      mouseStart, xScale,
                                      yScale, editState));
            }
            // If any return result is true, return true.
            foreach(bool result in returnResults)
            {
                if (result)
                    return true;
            }
            return false;
        }

        /*---------------------------------------------------------------------
                                  Private Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate member.
        /// </summary>
        private MouseDown mouseDownEvent = (
            ListView subImageListView, SubImageList subImages,
            MouseEventArgs e, Point mouseStart,
            float xScale, float yScale, string editState) => { return false; };
    }

    /// <summary>
    /// Contains a delegate which contains all the methods which process a
    /// MouseUp event. This is to allow a MouseUp event being handled
    /// based on the Editing State (whether it be Select, Rectangle, Polygon
    /// or Free Form).
    /// </summary>
    class MouseUpEvent
    {
        /*---------------------------------------------------------------------
                                  Private Methods
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate for all methods which handle a MouseUp Event
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public delegate bool MouseUp(ListView subImageListView,
                                     SubImageList subImages,
                                     MouseEventArgs e,
                                     Point mouseStart,
                                     float xScale, float yScale,
                                     string editState);

        /// <summary>
        /// Subscribes a method to the MouseUp delegate event.
        /// </summary>
        /// <param name="func">The method to subscribe</param>
        public void Subscribe(MouseUp func)
        {
            mouseUpEvent += func;
        }

        /// <summary>
        /// The method to call to trigger the MouseUp delegate event.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        public bool MouseUpTriggered(ListView subImageListView,
                                     SubImageList subImages,
                                     MouseEventArgs e,
                                     Point mouseStart,
                                     float xScale, float yScale,
                                     string editState)
        {
            // We need to read each of the return results, cause if one
            // evaluates to true, then the function should return true.
            // Create a list to store all the results.
            List<bool> returnResults = new List<bool>();

            // Iterate through each method and store it's result in the list.
            // Do this by getting the delegate's InvocationList.
            foreach (MouseUp up in mouseUpEvent.GetInvocationList())
            {
                returnResults.Add(up(subImageListView, subImages, e,
                                     mouseStart, xScale,
                                     yScale, editState));
            }
            // If any return result is true, return true.
            foreach (bool result in returnResults)
            {
                if (result)
                    return true;
            }
            return false;
        }

        /*---------------------------------------------------------------------
                                  Private Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate member.
        /// </summary>
        private MouseUp mouseUpEvent = (
            ListView subImageListView, SubImageList subImages,
            MouseEventArgs e, Point mouseStart,
            float xScale, float yScale, string editState) => { return false; };
    }

    /// <summary>
    /// Contains a delegate which contains all the methods which process a
    /// MouseMove event. This is to allow a MouseMove event being handled
    /// based on the Editing State (whether it be Select, Rectangle, Polygon
    /// or Free Form).
    /// </summary>
    class MouseMoveEvent
    {
        /*---------------------------------------------------------------------
                                  Private Methods
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate for all methods which handle a MouseMove Event
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public delegate bool MouseMove(ListView subImageListView,
                                       SubImageList subImages,
                                       MouseEventArgs e,
                                       Point mouseStart,
                                       float xScale, float yScale,
                                       string editState);

        /// <summary>
        /// Subscribes a method to the MouseUp delegate event.
        /// </summary>
        /// <param name="func">The method to subscribe</param>
        public void Subscribe(MouseMove func)
        {
            mouseMoveEvent += func;
        }

        /// <summary>
        /// The method to call to trigger the MouseMove delegate event.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        public bool MouseMoveTriggered(ListView subImageListView,
                                       SubImageList subImages,
                                       MouseEventArgs e,
                                       Point mouseStart,
                                       float xScale, float yScale,
                                       string editState)
        {
            // We need to read each of the return results, cause if one
            // evaluates to true, then the function should return true.
            // Create a list to store all the results.
            List<bool> returnResults = new List<bool>();

            // Iterate through each method and store it's result in the list.
            // Do this by getting the delegate's InvocationList.
            foreach (MouseMove move in mouseMoveEvent.GetInvocationList())
            {
                returnResults.Add(move(subImageListView, subImages, e,
                                       mouseStart, xScale,
                                       yScale, editState));
            }
            // If any return result is true, return true.
            foreach (bool result in returnResults)
            {
                if (result)
                    return true;
            }
            return false;
        }

        /*---------------------------------------------------------------------
                                  Private Members
        ---------------------------------------------------------------------*/
        /// <summary>
        /// The delegate member.
        /// </summary>
        private MouseMove mouseMoveEvent = (
            ListView subImageListView, SubImageList subImages,
            MouseEventArgs e, Point mouseStart,
            float xScale, float yScale, string editState) => { return false; };
    }
}
