﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    /// <summary>
    /// A Tool Strip which toggles buttons on or off. So that one and only one
    /// button is always selected at any one time.
    /// IMPORTANT: Make sure ALL items added to the toolstrip have their
    /// CheckOnClick property set to True.
    /// </summary>
    public partial class ToggleToolStrip : ToolStrip
    {
        /// <summary>
        /// Creates a new ToggleTooStrip.
        /// </summary>
        public ToggleToolStrip()
        {
            InitializeComponent();
            // Go through the items and find every button. Set it's
            // CheckOnClick property to true.
            foreach (ToolStripItem toolItem in this.Items)
            {
                // Ensure you deal with only buttons.
                if (toolItem.GetType() == typeof(ToolStripButton))
                {
                    // Set it's Check on Click property to true.
                    ToolStripButton button = (ToolStripButton)toolItem;
                    button.CheckOnClick = true;
                }
            }

        }

        /// <summary>
        /// Handles the drawing of the ToggleToolStrip. This just calls the
        /// base class.
        /// </summary>
        /// <param name="pe">The PaintEventArgs object</param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        /// <summary>
        /// Handles the ToggleToolStrip item clicked event. When a button is
        /// checked, the other buttons will become unchecked. If a button that
        /// was already checked is the one that was clicked, it will remain
        /// checked.
        /// </summary>
        /// <param name="sender">The object which sent the event</param>
        /// <param name="e">The ItemClicked event</param>
        private void ToggleToolStrip_ItemClicked(object sender,
                                                 ToolStripItemClickedEventArgs e)
        {
            // Go through the buttons. Uncheck the button that is checked
            // (except for the one that was clicked). If the clicked button is
            // the checked one, keep the button checked on.
            foreach (ToolStripItem toolItem in this.Items)
            {
                // Ensure you deal with only buttons.
                if (toolItem.GetType() == typeof(ToolStripButton))
                {
                    ToolStripButton button = (ToolStripButton)toolItem;
                    // Uncheck other button.
                    if (button.Checked && button != e.ClickedItem)
                    {
                        button.Checked = false;
                        break;
                    }
                    // Leave clicked button checked if it is the checked one by
                    // setting the check state (whether the button was pressed
                    // or not) to unchecked.
                    else if(button.Checked && button == e.ClickedItem)
                    {
                        button.CheckState = CheckState.Unchecked;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the name of the button that is checked. The possible return
        /// values are "editSelectButton", "editRectangleButton",
        /// "editPolygonButton", and "editFreeFormButton".
        /// </summary>
        /// <returns>Name of the button that is checked. Returns null if no
        /// button is checked.</returns>
        public string GetCheckedButtonName()
        {
            // Find the button that is checked.
            ToolStripButton checkedButton = null;
            foreach (ToolStripButton button in this.Items)
            {
                if (button.Checked)
                {
                    checkedButton = button;
                    break;
                }
            }

            // Return the checked button name
            return checkedButton.Name;
        }
    }
}
