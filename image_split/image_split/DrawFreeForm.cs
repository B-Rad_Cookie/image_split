﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace image_split
{
    /// <summary>
    /// Contains the handling of drawing a free-form boundary as a boundary of
    /// a sub-image. It handles the three stages required for drawing a
    /// polygon: The MouseDown, MouseMove and MouseUp.
    /// </summary>
    /// <remarks>
    /// As methods in this class are designed to be a delegate subscriber and
    /// this class is representing a editing "state", the purpose of the class
    /// is a utility class to provide functionalities for drawing polygons. As
    /// such, all members required in the drawing of a polygon will be a static
    /// on the class.
    /// </remarks>
    static class DrawFreeForm
    {
        /*---------------------------------------------------------------------
                              Public Static members
         --------------------------------------------------------------------*/
        /// <summary>
        /// The Polygon to draw.
        /// </summary>
        public static List<Point> drawnFreeForm;

        /// <summary>
        /// Determines whether a new polygon is being drawn.
        /// </summary>
        public static bool isDrawing = false;

        /// <summary>
        /// The MouseDown event handler for drawing a free-form boundary. This
        /// handles the creation of the first vertex of the boundary. It also
        /// resets the drawing. This method is designed to be subscribed to the
        /// MouseDownEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public static bool FreeFormMouseDownEvent(ListView subImageListView,
                                                  SubImageList subImages,
                                                  MouseEventArgs e,
                                                  Point mouseStart,
                                                  float xScale, float yScale,
                                                  string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the polygon button
            // "editFreeFormButton", do nothing.
            if (editState != "editFreeFormButton")
            {
                return false;
            }

            // Set the isDrawing to true
            DrawFreeForm.isDrawing = true;

            //  This is a new boundary, so reset the boundary to a new list.
            DrawFreeForm.drawnFreeForm = new List<Point>();

            // Add first point to the polygon.
            Point nextPoint = new Point((int)(e.Location.X * xScale),
                                        (int)(e.Location.Y * yScale));
            DrawFreeForm.drawnFreeForm.Add(nextPoint);

            // No redraw required.
            return false;
        }

        /// <summary>
        /// The MouseUp event handler for drawing a free-form boundary. Stops
        /// the drawing and saves the shape to the selected sub-image. This
        /// method is designed to be subscribed to the MouseUpEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        public static bool FreeFormMouseUpEvent(ListView subImageListView,
                                                 SubImageList subImages,
                                                 MouseEventArgs e,
                                                 Point mouseStart,
                                                 float xScale, float yScale,
                                                 string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the polygon button
            // "editFreeFormButton", do nothing.
            if (editState != "editFreeFormButton")
            {
                return false;
            }

            // Add the last point (if it is different to the last one).
            Point nextPoint = new Point((int)(e.Location.X * xScale),
                                         (int)(e.Location.Y * yScale));
            if (!DrawFreeForm.drawnFreeForm.Last().Equals(nextPoint))
                DrawFreeForm.drawnFreeForm.Add(nextPoint);

            // Save the information.
            SubImage selected =
                subImages.Find(subImageListView.SelectedItems[0].Text);
            selected.m_region = DrawFreeForm.drawnFreeForm;

            // Set the isDrawing to false
            DrawFreeForm.isDrawing = false;

            return true;
        }

        /// <summary>
        /// The MouseMove event handler for drawing a free-form boundary.
        /// Allows the boundary to be drawn. This method is designed to be
        /// subscribed to the MouseMoveEvent delegate.
        /// </summary>
        /// <param name="subImageListView">The ListView object containing the
        /// sub-image identifiers</param>
        /// <param name="subImages">The SubImageList containing the SubImage
        /// objects</param>
        /// <param name="e">The MouseEventArgs associated with the MouseDown
        /// </param>
        /// <param name="mouseStart">A point representing the location where
        /// the mouse was previously located.
        /// </param>
        /// <param name="xScale">The X scaling factor of the image being edited.
        /// </param>
        /// <param name="yScale">The Y scaling factor of the image being edited.
        /// </param>
        /// <param name="editState">A string representing the edit state 
        /// button that is selected.
        /// </param>
        /// <returns>A bool determining whether the PictureBox drawing sub
        /// images needs to be re-drawn.</returns>
        public static bool FreeFormMouseMoveEvent(ListView subImageListView,
                                                   SubImageList subImages,
                                                   MouseEventArgs e,
                                                   Point mouseStart,
                                                   float xScale, float yScale,
                                                   string editState)
        {
            // With all delegate methods, the first thing to do is the check
            // the edit state. If the edit state is not the polygon button
            // "editFreeFormButton", do nothing.
            if (editState != "editFreeFormButton")
            {
                return false;
            }

            // Mouse Move here is just simply adding another point.
            Point nextPoint = new Point((int)(e.Location.X * xScale),
                                        (int)(e.Location.Y * yScale));
            DrawFreeForm.drawnFreeForm.Add(nextPoint);

            // Re-draw the image canvas.
            return true;
        }
    }
}
